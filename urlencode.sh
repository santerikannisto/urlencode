#!/usr/bin/env sh
#
# Portable urlencode for urlencoding files
#
# Usage: urlencode.sh <file>
# Version: 0.1
# Created: 2018-06-03
# Contact: Santeri Kannisto <santeri.kannisto@webseodesigners.com>
# Public domain, 2018

_urlencode() {
  _str=$(cat "$1")
  printf "%s" "$_str" |
# convert newlines to audible bell so that that sed can handle the input without using non-POSIX extensions
  tr "\\r\\n" "\\a" |  
# urlencode characters
  sed -e 's/%/%25/g' -e 's/ /%20/g' -e 's/\!/%21/g' -e 's/"/%22/g' -e 's/#/%23/g' -e 's/\$/%24/g' -e 's/&/%26/g' -e 's/'\''/%27/g' -e 's/(/%28/g' -e 's/)/%29/g' -e 's/\*/%2A/g' -e 's/+/%2B/g' -e 's/,/%2C/g' -e 's/\./%2E/g' -e 's/\//%2F/g' -e 's/:/%3A/g' -e 's/;/%3B/g' -e 's/</%3C/g' -e 's/=/%3D/g' -e 's/>/%3E/g' -e 's/?/%3F/g' -e 's/@/%40/g' -e 's/\[/%5B/g' -e 's/\\/%5C/g' -e 's/\]/%5D/g' -e 's/\^/%5E/g' -e 's/_/%5F/g' -e 's/`/%60/g' -e 's/{/%7B/g' -e 's/|/%7C/g' -e 's/}/%7D/g' -e 's/~/%7E/g' -e 's/\a/%0A/g' --posix
}

if [ "$#" -ne 1 ]; then
  echo "Usage: urlencode.sh filename" >&2
  exit 1
fi

# urlencode <filename>

_urlencode "$1"